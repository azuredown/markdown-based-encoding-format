
String serializeMBEF(Map<String, dynamic> toEncode, {String header = ""}) {

	final StringBuffer buffer = StringBuffer("\nMBEF Version 2\n\n");

	if (header != "") {
		if (header.startsWith("##")) {
			throw Exception("Improper header format: must not start with ##");
		} else if (header.contains("\n##")) {
			throw Exception(r"Improper header format: must not contain \n##");
		}
		buffer.write(header);
		buffer.write("\n\n");
	}

	for (final MapEntry<String, dynamic> entry in toEncode.entries) {
		buffer.write("## ");
		buffer.write(sanitizeString(entry.key));
		buffer.write("\n");
		buffer.write(_serializeAnyType(entry.value, 0));
		buffer.write("\n\n");
	}

	return buffer.toString();
}

String _serializeAnyType(dynamic data, int currentDepth) {
	final StringBuffer buffer = StringBuffer();

	if (data == null) {
		buffer.write("null");
	} else if (data is String) {
		buffer.write(sanitizeString(data));
	} else if (data is bool) {
		buffer.write(data);
	} else if (data is int) {
		buffer.write(data);
	} else if (data is double) {
		buffer.write(data);
		buffer.write("d");
	} else if (data is List) {
		buffer.write("[");
		for (int i = 0; i < data.length; i++) {
			buffer.write(_serializePrimitiveType(data[i]));
			if (i != data.length - 1) {
				buffer.write(", ");
			}
		}
		buffer.write("]");
	} else if (data is Set) {
		buffer.write("(");
		int counter = 1;
		for (final dynamic item in data) {
			buffer.write(_serializePrimitiveType(item));
			if (counter != data.length) {
				counter++;
				buffer.write(", ");
			}
		}
		buffer.write(")");
	} else if (data is Map) {
		if (data.isEmpty) {
			buffer.write("\n");
			buffer.write("{}");
		} else {
			for (final MapEntry<dynamic, dynamic> entry in data.entries) {
				buffer.write("\n");
				writeIndents(buffer, currentDepth);
				buffer.write("- ");
				buffer.write(_serializePrimitiveType(entry.key));
				buffer.write(":");
				buffer.write(_serializeAnyType(entry.value, currentDepth + 1));
			}
		}
	} else {
		throw Exception("Unknown type: ${data.runtimeType}");
	}

	return buffer.toString();
}

void writeIndents(StringBuffer buffer, int numIndents) {
	for (int i = 0; i < numIndents; i++) {
		buffer.write("\t");
	}
}

String _serializePrimitiveType(dynamic data) {
	final StringBuffer buffer = StringBuffer();

	if (data is String) {
		buffer.write(sanitizeString(data));
	} else if (data is int) {
		buffer.write(data);
	} else if (data is double) {
		buffer.write(data);
		buffer.write("d");
	} else if (data is bool) {
		buffer.write(data);
	} else {
		throw Exception("Object: $data is not primitive!");
	}

	return buffer.toString();
}

String sanitizeString(String unsanitizedString) {
	if (int.tryParse(unsanitizedString) != null) {
		return "\"$unsanitizedString\"";
	} else if (unsanitizedString.endsWith("d") && double.tryParse(unsanitizedString.substring(0, unsanitizedString.length - 1)) != null) {
		return "\"$unsanitizedString\"";
	} else if (unsanitizedString.startsWith("- ") || unsanitizedString.startsWith("## ")) {
		return "\"$unsanitizedString\"";
	} else if (unsanitizedString == "" || unsanitizedString == "null" || unsanitizedString == "true" || unsanitizedString == "false") {
		return "\"$unsanitizedString\"";
	} else if ((unsanitizedString.startsWith("[") && unsanitizedString.endsWith("]")) || (unsanitizedString.startsWith("(") && unsanitizedString.endsWith(")")) || unsanitizedString == "{}") {
		return "\"$unsanitizedString\"";
	}
	return unsanitizedString.replaceAll("\"", "\\\"").replaceAll(", ", r"\,\ ").replaceAll("- ", r"\-\ ").replaceAll(":", r"\:").replaceAll("\n{}", r"\\n\{\}");
}
