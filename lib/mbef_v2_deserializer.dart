
import 'mbef_deserializer.dart';

Map<String, dynamic> deserializeMBEF(String toDeserialize) {

	if (!toDeserialize.contains("## ")) {
		return <String, dynamic>{};
	}

	while (true) {
		if (toDeserialize.startsWith("## ")) {
			break;
		} else if (toDeserialize.isEmpty) {
			throw Exception("Text is improperly formatted!");
		} else {
			toDeserialize = toDeserialize.substring(toDeserialize.indexOfUnescapedChar("\n") + 1);
		}
	}

	// Remove everything up to first header
	toDeserialize = toDeserialize.substring(toDeserialize.indexOfUnescapedChar("##"));

	final Map<String, String> rawData = <String, String>{};

	// The is not empty check is only applicable when going into the loop
	// the first time because there's a break for if toDeserialize is empty
	while (toDeserialize.isNotEmpty) {
		final int nextLineBreak = toDeserialize.indexOfUnescapedChar("\n");
		final String currentKey = toDeserialize.substring(3, nextLineBreak);
		toDeserialize = toDeserialize.substring(nextLineBreak + 1);
		final int nextHeader = toDeserialize.indexOfUnescapedChar("##");
		if (nextHeader == -1) {
			rawData[currentKey] = toDeserialize.substring(0, toDeserialize.length - 2);
			break;
		} else {
			rawData[currentKey] = toDeserialize.substring(0, nextHeader - 2);
			toDeserialize = toDeserialize.substring(nextHeader);
		}
	}

	final Map<String, dynamic> data = <String, dynamic>{};

	for (final MapEntry<String, String> entry in rawData.entries) {
		data[stringToData(entry.key, 0)] = stringToData(entry.value, 0);
	}

	return data;
}

dynamic stringToData(String value, int indentLevel) {
	if (value == "null") {
		return null;
	} else if (value.startsWith("\"") && value.endsWith("\"")) {
		return value.substring(1, value.length - 1);
	} else if (value == "true") {
		return true;
	} else if (value == "false") {
		return false;
	} else if (value == "\n{}") {
		return <dynamic, dynamic>{};
	} else if (value.startsWith("(") && value.endsWith(")")) {
		return stringToList(value.substring(1, value.length - 1), indentLevel).toSet();
	} else if (value.startsWith("[") && value.endsWith("]")) {
		return stringToList(value.substring(1, value.length - 1), indentLevel);
	// Must be last because can conflict with set and list
	} else if (value.contains("- ") && value.contains(":")) {
		return stringToMap(value, indentLevel);
	}

	final int? parsedValue = int.tryParse(value);
	if (parsedValue != null) {
		return parsedValue;
	}

	if (value.endsWith("d")) {
		final double? parsedValue = double.tryParse(value.substring(0, value.length - 1));
		if (parsedValue != null) {
			return parsedValue;
		}
	}

	return value.replaceAll("\\\"", "\"").replaceAll(r"\,\ ", ", ").replaceAll(r"\-\ ", "- ").replaceAll(r"\:", ":").replaceAll(r"\\n\{\}", "\n{}");
}

List<dynamic> stringToList(String string, int indentLevel) {

	if (string == "") {
		return <dynamic>[];
	}

	final List<String> rawContents = string.split(", ");
	final List<dynamic> contents = <dynamic>[];

	for (int i = 0; i < rawContents.length; i++) {
		contents.add(stringToData(rawContents[i], indentLevel));
	}

	return contents;
}

Map<dynamic, dynamic> stringToMap(String string, int indentLevel) {
	final List<String> rawContents = string.split("\n${"\t" * indentLevel}- ");

	// It starts with a '\n- ' so the first value will always be empty
	rawContents.removeAt(0);

	final Map<dynamic, dynamic> finalContents = <dynamic, dynamic>{};
	for (int i = 0; i < rawContents.length; i++) {
		final int delimiterLocation = rawContents[i].indexOfUnescapedChar(":");
		final String item1 = rawContents[i].substring(0, delimiterLocation);
		final String item2 = rawContents[i].substring(delimiterLocation + 1);
		finalContents[stringToData(item1, indentLevel + 1)] = stringToData(item2, indentLevel + 1);
	}

	return finalContents;
}
