
import 'package:binary_codec/binary_codec.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:markdown_based_encoding_format/mbef_deserializer.dart';
import 'package:markdown_based_encoding_format/mbef_serializer.dart';

void main() {

	test("Empty", () {
		testGivenData(<String, dynamic>{});
	});

	test("Top Level And Primitive", () {
		testGivenData(<String, dynamic>{ "Hello" : null, "Bye" : 32 });
		testGivenData(<String, dynamic>{
			"1" : "null",
			"2" : "true",
			"3" : "false",
			"4" : "",
			"5" : "\"\"",
			"6" : "\"\"",
			"7" : "1",
			"8" : "1.2",
			"9" : "1.2d",
			":" : ":",
			"\"\"\"" : "22",
			"" : 4,
			"null" : 123.123,
			"true" : "false",
		});
		testGivenData(<String, dynamic>{
			"1" : "[]",
			"2" : "()",
			"3" : "{}",
			"4" : "{\n}",
			"5" : "\n",
			"6" : ", , ",
			"7" : ", ",
			"8" : r"\, ",
			"9" : "\\\"",
			"10" : "[1, 2, 3]",
		});
		testGivenData(<String, dynamic>{ "False" : true, "True" : false });
		testGivenData(<String, dynamic>{ "Double" : 123.456, "11" : "\n{}"});
	});

	test("Top Level And Empty", () {
		testGivenData(<String, dynamic>{
			"List" : <dynamic>[],
			"Map" : <dynamic, dynamic>{},
		});
		testGivenData(<String, dynamic>{
			"Set" : <dynamic>{},
		}, useBinaryCodec: false);
	});

	test("Nested 1 Level list", () {
		testGivenData(<String, dynamic>{
			"1" : <dynamic>[1, 2, 3],
		});
		testGivenData(<String, dynamic>{
			"2" : <dynamic>[", ", ", "],
		});
		testGivenData(<String, dynamic>{
			"3" : <dynamic>[r"test\", ", "],
		});
		testGivenData(<String, dynamic>{
			"" : <dynamic>[r"test\", "false", "[]"],
		});
	});

	test("Nested 1 Level set", () {
		testGivenData(<String, dynamic>{
			"1" : <dynamic>{1, 2, 3},
		}, useBinaryCodec: false);
		testGivenData(<String, dynamic>{
			"2" : <dynamic>{", ", "a"},
		}, useBinaryCodec: false);
		testGivenData(<String, dynamic>{
			"3" : <dynamic>{"a", ", "},
		}, useBinaryCodec: false);
		testGivenData(<String, dynamic>{
			"null" : <dynamic>{r"test\", ", ", "true"},
		}, useBinaryCodec: false);
	});

	group("Escaped String", () {
		test("Escaped String Simple", () {
			expect("Hello".indexOfUnescapedChar("Hello"), 0);
			expect("Hello".indexOfUnescapedChar("l"), 2);
			expect("Hello".indexOfUnescapedChar("a"), -1);
		});

		test("Escaped String Escape", () {
			expect(r"He\llo".indexOfUnescapedChar("l"), 4);
			expect(r"\llo".indexOfUnescapedChar("l"), 2);
			expect(r"\\\llo".indexOfUnescapedChar("l"), 4);
			expect(r"\\\\\llo".indexOfUnescapedChar("l"), 6);
		});

		test("Escaped String False Escape", () {
			expect(r"He\\llo".indexOfUnescapedChar("l"), 4);
			expect(r"\\llo".indexOfUnescapedChar("l"), 2);
			expect(r"\\\\llo".indexOfUnescapedChar("l"), 4);
		});

		test("Escaped String Multiescapes", () {
			expect(r"\\\l\\lo".indexOfUnescapedChar("l"), 6);
			expect(r"\\\\\l\\\lo".indexOfUnescapedChar("l"), -1);
		});
	});

	test("Nested 1 Level map", () {
		testGivenData(<String, dynamic>{
			"1" : <dynamic, dynamic>{ 1 : 2 },
		});
		testGivenData(<String, dynamic>{
			"1" : <dynamic, dynamic>{ 1 : 2 },
			"2" : <dynamic, dynamic>{ 1 : 2 },
		});
		testGivenData(<String, dynamic>{
			"true" : <dynamic, dynamic>{ "false" : "true" },
			"false" : <dynamic, dynamic>{ "false" : true },
			"" : <dynamic, dynamic>{ "null" : "" },
		});
		testGivenData(<String, dynamic>{
			"1" : <dynamic, dynamic>{ ": " : "\n- " },
			"2" : <dynamic, dynamic>{ "\n- " : ": " },
			"3" : <dynamic, dynamic>{ ":" : ":" },
			"4" : <dynamic, dynamic>{ " : " : " : " },
		});
	});

	test("Colon space", () {
		testGivenData(<String, dynamic>{
			"Articles" : <dynamic, dynamic>{
				"Windows 11: 111% A Cash Grab" : <dynamic, dynamic>{
					2 : 3,
				},
				"Windows: 111% A Cash Grab" : <dynamic, dynamic>{
					2 : 3,
				},
			},
		});
	});

	test("Recursive maps", () {
		testGivenData(<String, dynamic>{
			"1" : <dynamic, dynamic>{
				1 : <dynamic, dynamic>{
					2 : 3,
				},
			},
		});
		testGivenData(<String, dynamic>{
			"1" : <dynamic, dynamic>{
				1 : <dynamic, dynamic>{
					2 : 3,
					"Hello" : "Bye",
					"null" : "[]",
					"test: thing" : "Apple",
				},
				"true" : <dynamic, dynamic>{
					true : <dynamic, dynamic>{
						4 : 5,
					},
					"\n\t- " : <dynamic, dynamic>{
						"\n\t- " : "\n\t- ",
						"\n\t\t- " : "\n\t\t- ",
					},
				},
			},
		});
	});

	test("BlogArticleMetadata (From My App)", () {
		testGivenData(<String, dynamic>{
			"Tags" : <dynamic, dynamic>{},
			"Categories" : <dynamic, dynamic>{},
			"Articles" : <dynamic>[],
		}, header: "This file is supposed to be checked into version control. Do not attempt to edit this file manually without a backup.");
		// Tags as list
		testGivenData(<String, dynamic>{
			"Tags" : <dynamic, dynamic>{ 5 : "Cars" },
			"Categories" : <dynamic, dynamic>{ 0 : "XYZ", 2 : "Current Events" },
			"Articles" : <dynamic, dynamic> {
				"Article1" : <dynamic, dynamic> {
					"Article Title" : "Article1",
					"Display Name" : null,
					"Article ID" : 18432,
					"Category ID" : null,
					"Tags" : <dynamic>[],
					"Publish Time" : 9281429,
					"Linked Stories" : <dynamic>[],
					"URL" : null,
				},
				"Article2" : <dynamic, dynamic> {
					"Article Title" : "Article2",
					"Display Name" : null,
					"Article ID" : 985883,
					"Category ID" : 2,
					"Tags" : <dynamic>[1, 3, 4],
					"Publish Time" : 9281429,
					"Linked Stories" : <dynamic>[94123, 324532, 12324],
					"URL" : "www.example.com",
				},
				"" : <dynamic, dynamic> {
					"Article Title" : null,
					"Display Name" : null,
					"Article ID" : 985883,
					"Category ID" : null,
					"Tags" : <dynamic>[1, 3, 4],
					"Publish Time" : 9281429,
					"Linked Stories" : <dynamic>[94123, 324532, 12324],
					"URL" : "www.example.com",
				},
			},
		}, header: "This file is supposed to be checked into version control. Do not attempt to edit this file manually without a backup.");
		// Tags as set
		testGivenData(<String, dynamic>{
			"Tags" : <dynamic, dynamic>{ 5 : "Cars" },
			"Categories" : <dynamic, dynamic>{ 0 : "XYZ", 2 : "Current Events" },
			"Articles" : <dynamic, dynamic> {
				"Article1" : <dynamic, dynamic> {
					"Article Title" : "Article1",
					"Display Name" : null,
					"Article ID" : 18432,
					"Category ID" : null,
					"Tags" : <dynamic>{},
					"Publish Time" : 9281429,
					"Linked Stories" : <dynamic>[],
					"URL" : null,
				},
				"Article2" : <dynamic, dynamic> {
					"Article Title" : "Article2",
					"Display Name" : null,
					"Article ID" : 985883,
					"Category ID" : 2,
					"Tags" : <dynamic>{1, 3, 4},
					"Publish Time" : 9281429,
					"Linked Stories" : <dynamic>[94123, 324532, 12324],
					"URL" : "www.example.com",
				},
				"Article3" : <dynamic, dynamic> {
					"Article Title" : "Article3",
					"Display Name" : null,
					"Article ID" : 985883,
					"Category ID" : 2,
					"Tags" : <dynamic>{1, 3, 4},
					"Publish Time" : 9281429,
					"Linked Stories" : <dynamic>[94123, 324532, 12324],
					"URL" : "www.example.com",
				},
			},
		}, useBinaryCodec: false);
		testGivenData(<String, dynamic>{
			"Tags" : <dynamic, dynamic>{ 5 : "Cars" },
			"Categories" : <dynamic, dynamic>{ 0 : "XYZ", 2 : "Current Events" },
			"Articles" : <dynamic, dynamic> {
				" 1" : <dynamic, dynamic> {
					"Article Title" : "Article1",
					"Display Name" : null,
					"Article ID" : 18432,
					"Category ID" : null,
					"Tags" : <dynamic>{},
					"Publish Time" : 9281429,
					"Linked Stories" : <dynamic>[],
					"URL" : null,
				},
				" 2" : <dynamic, dynamic> {
					"Article Title" : "Article2",
					"Display Name" : null,
					"Article ID" : 985883,
					"Category ID" : 2,
					"Tags" : <dynamic>{1, 3, 4},
					"Publish Time" : 9281429,
					"Linked Stories" : <dynamic>[94123, 324532, 12324],
					"URL" : "www.example.com",
				},
			},
		}, useBinaryCodec: false);
		testGivenData(<String, dynamic>{
			"Tags" : <dynamic, dynamic>{ 5 : "Cars" },
			"Categories" : <dynamic, dynamic>{ 0 : "XYZ", 2 : "Current Events" },
			"Articles" : <dynamic, dynamic> {
				"Article1" : <dynamic, dynamic> {
					"Article Title" : "Article1",
					"Display Name" : null,
					"Article ID" : 18432,
					"Category ID" : null,
					"Tags" : <dynamic>{},
					"Publish Time" : 9281429,
					"Linked Stories" : <dynamic>[],
					"URL" : null,
				},
			},
		}, useBinaryCodec: false);
	});
}

void testGivenData(Map<String, dynamic> data, {bool useBinaryCodec = true, String header = ""}) {

	String? errorMessageOnList(List<dynamic> list1, List<dynamic> list2) {
		if (list1.length != list2.length) {
			return "List sizes differ: ${list1.length} and ${list2.length}";
		} else {
			for (int i = 0; i < list1.length; i++) {
				if (list1[i].runtimeType != list2[i].runtimeType) {
					return "Type mismatch: ${list1[i].runtimeType} and ${list2[i].runtimeType}";
				}
			}
		}

		return null;
	}

	String? errorMessageOnMap(Map<dynamic, dynamic> item1, Map<dynamic, dynamic> item2) {

		for (final dynamic key in item1.keys) {
			if (!item1.containsKey(key)) {
				return "Unable to find key: $key";
			} else if (item1[key].runtimeType != item2[key].runtimeType) {
				return "Type mismatch at key $key: ${item2[key].runtimeType} and ${item1[key].runtimeType}";
			}
			if (item1[key] is List) {
				final String? possibleError = errorMessageOnList(item1[key], item2[key]);
				if (possibleError != null) {
					return "At key $key: $possibleError";
				}
			} else if (item1[key] is Set) {
				final String? possibleError = errorMessageOnList(List<dynamic>.from(item1[key]), List<dynamic>.from(item2[key]));
				if (possibleError != null) {
					return "At key $key: $possibleError";
				}
			} else if (item1[key] is Map) {
				final String? possibleError = errorMessageOnMap(item1[key], item2[key]);
				if (possibleError != null) {
					return "At key $key: $possibleError";
				}
			}
		}

		return null;
	}

	final Map<String, dynamic> result = deserializeMBEF(serializeMBEF(data, header: header));

	expect(
		useBinaryCodec ? binaryCodec.encode(result).toString() : result.toString(),
		useBinaryCodec ? binaryCodec.encode(data).toString() : data.toString(),
		reason: "Data: $data Result: $result ${errorMessageOnMap(data, result) ?? "Unknown error"}"
	);

	final String? message = errorMessageOnMap(data, result);
	if (message != null) {
		fail(message);
	}
}
